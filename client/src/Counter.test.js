import React from 'react'
import { cleanup, fireEvent, render } from '@testing-library/react'
import Counter from './Counter'

afterEach(cleanup)

test('<Counter />', () => {
    const wrapper = render(<Counter />)
    wrapper.debug()
    const num = wrapper.getByTestId('counter-button').textContent
    expect(num).toBe('0')
    const elementType = wrapper.getByTestId('counter-button').tagName
    console.log('tagName is ', elementType)
    expect(elementType).toBe('BUTTON')
})

// Can use destructuring
test('<Counter />', () => {
    const { debug, getByTestId } = render(<Counter />)
    const counterButton = getByTestId('counter-button')
    debug()

    // Check to make sure counter is 0 or greater
    var numStr = counterButton.textContent
    var num = Number(numStr)
    var numIsZeroOrGreater = num >= 0 ? true : false
    expect(numIsZeroOrGreater).toBeTruthy()

    // Check to make sure counter starts at 0
    numStr = counterButton.textContent
    num = Number(numStr)
    const numIsZero = num === 0 ? true : false
    expect(numIsZero).toBeTruthy()

    // Check that there is a button
    const elementType = counterButton.tagName
    expect(elementType).toBe('BUTTON')

    // Click the button and make sure it goes to 1
    fireEvent.click(counterButton)
    numStr = counterButton.textContent
    num = Number(numStr)
    expect(num).toBe(1)

    // Click the button twice more and make sure it goes to 3
    fireEvent.click(counterButton)
    fireEvent.click(counterButton)
    numStr = counterButton.textContent
    num = Number(numStr)
    expect(num).toBe(3)

    debug()
})