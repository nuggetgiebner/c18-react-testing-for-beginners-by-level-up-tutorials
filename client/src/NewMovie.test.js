import React from 'react'
import { cleanup, fireEvent, render } from '@testing-library/react'
import NewMovie from './NewMovie'

afterEach(cleanup)

test('NewMovie', () => {
    const { container, debug, getByTestId, getByText } = render(<NewMovie />)
    // debug()

    // Look for an h1
    const h1Text = getByTestId('NewMovie-h1').textContent
    expect(h1Text).toContain('New Movie')

    // Look for form
    const newMovieForm = getByTestId('MovieForm-form')
    expect(newMovieForm.nodeName).toBe('FORM')

    expect(container.firstChild).toMatchSnapshot()

})