import React from 'react'
import { cleanup, fireEvent, render } from '@testing-library/react'
import MovieForm from './MovieForm'

afterEach(cleanup)

const onSubmit = jest.fn()

test('MovieForm', () => {
    const { getByTestId, getByLabelText, getByText } = render(<MovieForm submitForm={onSubmit} />)
    // debug()

    // Look for form
    const newMovieForm = getByTestId('MovieForm-form')
    expect(newMovieForm.nodeName).toBe('FORM')

    // getByLabelText('Text').value = 'hello'
    // fireEvent.change(getByLabelText('Text'))

    fireEvent.change(getByLabelText('Text'), {
        target: { value: 'hello' }
    })

    fireEvent.click(getByText('Submit'))
    
    expect(onSubmit).toHaveBeenCalledTimes(1)
    expect(onSubmit).toHaveBeenCalledWith({
        text: 'hello'
    })
})